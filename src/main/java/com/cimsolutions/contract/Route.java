package com.cimsolutions.contract;

import java.util.List;

public class Route {
	
	private int id; 
	private List<Node> route;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Node> getRoute() {
		return route;
	}

	public void setRoute(List<Node> route) {
		this.route = route;
	}

	
}
