package com.cimsolutions.contract;

import java.util.Map;

public class Node {
	
	private String naam;
	private String straat;
	private String postcode;
	private String plaats;
	private Map<Node, Integer> bestemmingen;
	
	public String getNaam() {
		return naam;
	}
	public void setNaam(String Naam) {
		this.naam = Naam;
	}
	public String getStraat() {
		return straat;
	}
	public void setStraat(String Straat) {
		this.straat = Straat;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String Postcode) {
		this.postcode = Postcode;
	}
	public String getPlaats() {
		return plaats;
	}
	public void setPlaats(String Plaats) {
		this.plaats = Plaats;
	}
	public Map<Node, Integer> getBestemmingen() {
		return bestemmingen;
	}
	public void setBestemmingen(Map<Node, Integer> Bestemmingen) {
		this.bestemmingen = Bestemmingen;
	}
	
	
}
