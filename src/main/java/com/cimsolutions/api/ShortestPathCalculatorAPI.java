package com.cimsolutions.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cimsolutions.contract.Route;
import com.cimsolutions.service.ShortestPathCalculatorService;

@RestController
@RequestMapping("/api/routes/")
public class ShortestPathCalculatorAPI {
	
	@Autowired
	private ShortestPathCalculatorService shortestPathCalculator;
	
	@GetMapping("ping")
	public String ping() {
		return "OK";
	}
	
	@GetMapping("list")
	public Route getBestRoute() {
		Route bestRoute = shortestPathCalculator.calculatePath();
		
		bestRoute.setId(9);
		
		return bestRoute;
	}
	
	public void setShortestPathCalculatorService(ShortestPathCalculatorService shortestPathCalculatorService) {
		this.shortestPathCalculator = shortestPathCalculatorService;
	}
}
