package com.cimsolutions;

import java.util.Map;
import java.util.TreeMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cimsolutions.api.ShortestPathCalculatorAPI;
import com.cimsolutions.service.ShortestPathCalculatorService;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
    	
        SpringApplication.run(Main.class, args);
        //ShortestPathCalculatorAPI api = new ShortestPathCalculatorAPI();
        //ShortestPathCalculatorService service = new ShortestPathCalculatorService();
        //api.setShortestPathCalculatorService(service);
        //api.getBestRoute();
    }
}         


