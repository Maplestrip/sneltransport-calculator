package com.cimsolutions.infra;

import java.util.List;

import com.cimsolutions.contract.Node;

import feign.RequestLine;

public interface RouteServiceClient {
	
	@RequestLine(value="GET /api/matrix/?leverDatum=12-6-2018")
	List<Node> getNodes();
}
