package com.cimsolutions.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cimsolutions.contract.Route;
import com.cimsolutions.contract.Node;
import com.cimsolutions.infra.RouteServiceClient;

import feign.Feign;
import feign.gson.GsonDecoder;

@Service
public class ShortestPathCalculatorService {
	
	private RouteServiceClient routeServiceClient = Feign.builder()
            .decoder(new GsonDecoder())
            .target(RouteServiceClient.class, "http://172.16.1.176/sneltransportapi");
	
	public Route calculatePath() {
		
		List<Node> nodes = new ArrayList<>(); //routeServiceClient.getNodes();
		Node node = new Node();
		node.setNaam("Some Name");
		node.setPlaats("Some Flat");
		
		nodes.add(node);
		
		
		System.out.println(nodes);
		
		Route route = new Route();
		route.setRoute(nodes);
		
		return route;
	}

}
