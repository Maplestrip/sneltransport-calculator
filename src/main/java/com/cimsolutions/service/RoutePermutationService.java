package com.cimsolutions.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import com.cimsolutions.contract.Vertex;

public class RoutePermutationService
{
	private ArrayList<Vertex> vertices;
	private String[] locaties; //Wordt vervangen met Nodes
	private int longestAllowedPath;
	private static ArrayList<Stack<String>> options = new ArrayList<Stack<String>>();
	
	protected ArrayList<String> getShortestPath(ArrayList<Vertex> vertices, String[] locaties, int longestAllowedPath)
	{
		this.vertices = vertices;
		this.locaties = locaties;
		this.longestAllowedPath = longestAllowedPath;
		ArrayList<String> route = new ArrayList<String>();
		
		//route.add("snel");
		
		Set<String> locatieSet = new HashSet<String>(Arrays.asList(locaties));
		AllPossibleRoutes(locatieSet, new Stack<String>(), locaties.length);

	    route = OptimalRoute(route);
	    
		//route.add("snel");
		
		return route;
	}
	
	private void AllPossibleRoutes(Set<String> locatieSet, Stack<String> permutation, int size) 
	{		
		if(permutation.size() == size) 
		{
	        /* add the permutation to the list of options */
			//options.add(permutation);
			options.add((Stack<String>)permutation.clone());
			//System.out.println(Arrays.toString(options.get(options.size() - 1).toArray(new String[0])));
			
	    }
		else
		{
			Set<String> tempSet = new HashSet<String>();
			tempSet.addAll(locatieSet);
			/* items available for permutation */
			for(String locatie : locatieSet) 
			{
				/* add current item */
				permutation.push(locatie);

				/* remove item from available item set */
				tempSet.remove(locatie);

				/* pass it on for next permutation */
				AllPossibleRoutes(tempSet, permutation, size);

				/* pop and put the removed item back */
				tempSet.add(permutation.pop());
			}
		}
		//return options;
	}
	
	private ArrayList<String> OptimalRoute(ArrayList<String> route)
	{
		for(Stack<String> option : options)
		{
			int costOfOption = 0;
			int numberOfOptions = option.size();
			int optionNumber = 0;
			while(optionNumber < numberOfOptions - 1)
			{
				costOfOption += GetCost(option.get(optionNumber), option.get(optionNumber + 1));
				
				optionNumber++;
			}
			if(costOfOption > longestAllowedPath)
			{
				//remove the option from the list.
				//options.remove(option);
			}
			
			System.out.println(costOfOption);
			System.out.println(Arrays.toString(option.toArray(new String[0])));
		}
		
		return route;
	}
	
	private int GetCost(String source, String destination)
	{
		for(Vertex vertex : vertices)
		{
			//Check for every vertex that touches upon the current location.
			if(vertex.getSource().equals(source))
			{
				if(vertex.getDestination().equals(destination))
				{
					return vertex.getCost();
				}
			}
			if(vertex.getDestination().equals(source))
			{
				if(vertex.getSource().equals(destination))
				{
					return vertex.getCost();
				}
			}
		}
		System.out.print("test");
		return 9999;
	}
	
	/*
	private HashMap<String, HashMap> AllPossibleRoutesFromHere(ArrayList<String> route, HashMap costTable)
	{
		HashMap<String, HashMap> routeOptions = new HashMap<String, HashMap>();
		Set<Entry> entries = costTable.entrySet();
		ArrayList<String> tempRoute = new ArrayList<String>();
		tempRoute.addAll(route);
		
		Collection temp = costTable.values();
		
		//Take each route in the costTable, and return a new costTable
		if(costTable.values().iterator().next() instanceof Integer)
		{
			for(Map.Entry<String, Integer> entry : entries)
			{
				System.out.println(entry.getKey());
				route.add(entry.getKey());
				costTable = GetCostsForCurrentLocation(route);
				route.remove(route.size() - 1);
				routeOptions.put(entry.getKey(), costTable);
			}
		}
		else
		{
			for(Map.Entry<String, HashMap> entry : entries)
			{
				if(!route.contains(entry.getKey()))
				{
					route.add(entry.getKey());
				}
				routeOptions.putAll(costTable);
			}
		}
		System.out.println(route);
		//System.out.println(routeOptions);
		return routeOptions;
	}
	
	
	/*
	private ArrayList<ArrayList<String>> AllPossibleRoutes(ArrayList<ArrayList<String>> routes)
	{
		ArrayList<String> sublist = new ArrayList();
		for(String locatie : locaties)
		{
			if(!routes.contains(locatie))
			{
				if(!sublist.contains(locatie))
					sublist.add(locatie);
			}
			else if(!routes.get(0).contains(locatie))
			{
				if(!sublist.contains(locatie))
					sublist.add(locatie);
			}
			else if(!routes.get(1).contains(locatie))
			{
				if(!sublist.contains(locatie))
					sublist.add(locatie);
			}
			else if(!routes.get(2).contains(locatie))
			{
				if(!sublist.contains(locatie))
					sublist.add(locatie);
			}
			else if(!routes.get(3).contains(locatie))
			{
				if(!sublist.contains(locatie))
					sublist.add(locatie);
			}
			System.out.println(locatie);
		}
		
		routes.add(sublist);
		return routes;
	}
	*/
}
